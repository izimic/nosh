const std = @import("std");
const environement = @import("./environement.zig");
const builtin = @import("./builtin.zig");
var gpa = std.heap.GeneralPurposeAllocator(.{}){};

fn wexitstatus(val: u32) u8 {
  return @truncate(u8, val >> 8);
}

pub const Variable = struct {
  str :[:0]u8,

  pub fn init(str: [:0]u8) Variable {
    return Variable {
      .str = str,
    };
  }

  pub fn evaluate(self: Variable,env: *environement.Environement) [:0]const u8 {
    var split = std.mem.split(u8,self.str,".");
    if (split.next()) |*tok| {
      if (std.mem.eql(u8,tok.*,"")) tok.* = ".";
      var variable = env.get(tok.*);
      while (split.next()) |token| {
        switch (variable) {
          .Object => |object| {
            if (object.get(token)) |val| {
              variable = val;
            }
            else {
              return "undefined";
            }
          },
          else => return "undefined",
        } 
      }

      switch (variable) {
        .Bool => |val| { if (val) return "true" else return "false";},
        .Integer => |val| {
          var buf = [_:0]u8{0} ** 32;
          var r = std.fmt.bufPrint(buf[0..],"{}",.{val}) catch "0";
          return r[0..:0];
        },
        .Float => |val| {
          var buf = [_:0]u8{0} ** 32;
          var r = std.fmt.bufPrint(buf[0..],"{}",.{val}) catch "0.";
          return r[0..:0];
        },
        .String,.NumberString => |str| {
          var r = gpa.allocator().alloc(u8,str.len + 1) catch "";
          std.mem.copy(u8,r,str);
          r[str.len] = 0;
          return r[0..str.len:0];
        },
        .Object => return "{Object}",
        .Array => return "[Array]",
        .Null => return "null",
      }
    }
    else return "undefined";
  }
};

pub const Value = union(enum) {
  id: [:0]u8,
  variable: Variable,
  
  pub fn evaluate(self: Value, env: *environement.Environement) [:0]const u8 {
    switch(self) {
      Value.id => |str| return str,
      Value.variable => |variable| return variable.evaluate(env),
    }
  }
};

pub const Block = struct {
  instructions: []Instruction,

  pub fn init(instructions: []Instruction) Block {
    return Block {
      .instructions = instructions,
    };
  }

  pub fn exec(self: *Block, env: *environement.Environement) anyerror!void {
    var val: u32 = 0;
    for(self.instructions) |*ins| {
      val = try ins.exec(env);
    }
    std.os.exit(wexitstatus(val));
    unreachable;
  }

  pub fn exec_alone(self: *Block, env: *environement.Environement) anyerror!u32 {
    var pid = try std.os.fork();
    if (pid == 0) {
      try self.exec(env);
      unreachable;
    }
    else {
      var r = std.os.waitpid(pid,0);
      return r.status;
    }
  }

  pub fn evaluate(self: *Block, env: *environement.Environement) [:0]const u8 {
    //TODO
    _ = self;
    _ = env;
    return "";
  }
};

pub const Arg = union(enum) {
  value: Value,
  block: Block,

  pub fn evaluate(self: *Arg, env: *environement.Environement) [:0]const u8 {
    switch(self.*) {
      Arg.value => |*value| return value.evaluate(env),
      Arg.block => |*block| return block.evaluate(env)
    }
  }
};

pub const Command = struct {
  name: Value,
  args: [] Arg,

  pub fn init(name: Value, args: [] Arg ) Command {
    return Command {
      .name = name,
      .args = args,
    };
  }

  pub fn exec(self: *Command, env: *environement.Environement) !void {
    const name = self.name.evaluate(env);
    if (try builtin.exec(name,self.args,env)) {
      std.os.exit(0);
      return;
    }
 
    const allocator = std.heap.page_allocator;
    const len = self.args.len + 2;
    const args = (try allocator.alloc(?[*:0]const u8, len));

    var i:u32 = 1;
    args[0] = name.ptr;
    for (self.args) |*arg| {
      var value = arg.evaluate(env);
      args[i] = value.ptr;
      i += 1;
    }
    args[i] = null;

    var envp = try env.toPtr();
    
    switch(std.os.execvpeZ(args[0].?,args[0..args.len-1:null].ptr, envp)) {
      else => |err| return err,
    }
  }

  pub fn exec_alone(self: *Command, env: *environement.Environement) !u32 {
    const name = self.name.evaluate(env);
    if (try builtin.exec(name,self.args,env)) {
      return 0;
    }
    else {
      var pid = try std.os.fork();
      if (pid == 0) {
        try self.exec(env);
        return 1;
      }
      else {
        var r = std.os.waitpid(pid,0);
        return r.status;
      }
    }
  }
};

pub const BlockOrCommand = union(enum) {
  command: Command,
  block: Block,

  pub fn exec(self: *BlockOrCommand, env: *environement.Environement) !void {
    switch(self.*) {
      BlockOrCommand.command => |*command| return command.exec(env),
      BlockOrCommand.block => |*block| return block.exec(env),
    }
  }
  
  pub fn exec_alone(self: *BlockOrCommand, env: *environement.Environement) !u32 {
    switch(self.*) {
      BlockOrCommand.command => |*command| return command.exec_alone(env),
      BlockOrCommand.block => |*block| return block.exec_alone(env),
    }
  }
};

pub const Process = struct {
  cmd: BlockOrCommand,
  pid: i32 = 0,

  pub fn init(cmd: BlockOrCommand) Process {
    return Process {
      .cmd = cmd,
    };
  }

  pub fn exec(self: *Process, env: *environement.Environement) !void {
    return self.cmd.exec(env);
  }

  pub fn exec_alone(self: *Process, env: *environement.Environement) !u32 {
    return self.cmd.exec_alone(env);
  }
};

pub const Pipe = struct {
   process : []Process,
   
   pub fn init(process: []Process) Pipe {
     return Pipe {
       .process = process,
     };
   }

   pub fn exec(self: *Pipe, env: *environement.Environement) !u32 {
     var input : i32 = 0;

     if (self.process.len == 1) {
       return self.process[0].exec_alone(env);
     }

     //pipe/fork all proces
     for(self.process) |*cmd| {
       var fds = try std.os.pipe();
       var pid = try std.os.fork();
       if (pid == 0) {
         std.os.close(fds[0]);
         if (input != 0) {
           std.os.close(0);
           _ = try std.os.dup(input);
         }
         std.os.close(1);
         _ = try std.os.dup(fds[1]);
         try cmd.exec(env);
       }
       else {
         std.os.close(fds[1]);
         input = fds[0];
         cmd.pid = pid;
       }
     }

     //fork implicite viewer
     var pid = try std.os.fork();
     if (pid == 0) {
       std.os.close(0);
       _ = try std.os.dup(input);
       
       var args = [2]?[*:0]const u8{"userland/viewer",null};
       var envp = [1]?[*:0]const u8{null};
       
       switch(std.os.execvpeZ(args[0].?,args[0..1:null], envp[0..0:null])) {
         else => |err| return err,
       }
       unreachable;
     }

     //wait all proces
     var status:u32 = 0;
     for(self.process) |c| {
       if(c.pid != 0 ) {
         var r = std.os.waitpid(c.pid,0);
         if (r.pid == pid) {
           status = r.status;
         }
       }
     }

    //wait implicite viewer
     _ = std.os.waitpid(pid,0);

     return status;
   }
};

pub const CmdInstruction = struct {
  process: Pipe,
  background: bool = false,
  op: union(enum) {
    orCmd: *CmdInstruction,
    andCmd: *CmdInstruction,
    Null: void
  },

  pub fn exec(self: *CmdInstruction, env: *environement.Environement) !u32 {
    switch(self.op) {
      .orCmd => |cmd| {
        var r = try self.process.exec(env);
        return if(r == 0) r else cmd.exec(env);
      },
      .andCmd => |cmd| {
        var r = try self.process.exec(env);
        return if (r == 0) cmd.exec(env) else r;
      },
      .Null => return self.process.exec(env),   
    }
  }
};

pub const IfInstruction = struct {
  condition: CmdInstruction,
  instruction: *Instruction,

  pub fn init(condition: CmdInstruction, instruction: *Instruction) IfInstruction {
    return IfInstruction {
      .condition = condition,
      .instruction = instruction,
    };
  }

  pub fn exec(self: *IfInstruction, env: *environement.Environement) !u32 {
    if((try self.condition.exec(env)) == 0) return self.instruction.exec(env);
    return @as(u32,1);
  }
};

pub const Assignement = struct {
  name: [:0]u8,
  value: Value,

  pub fn init(name: [:0]u8, value: Value) Assignement {
    return Assignement {
      .name = name,
      .value = value,
    };
  }

  pub fn exec(self: *Assignement, env: *environement.Environement) !u32 {
    try env.put_local(self.name,.{.String = self.value.evaluate(env)});
    return 0;
  }
};

pub const Instruction = union(enum) {
  cmd: CmdInstruction,
  assignement: Assignement,
  ifInstruction: IfInstruction,

  pub fn exec(self: *Instruction, env: *environement.Environement) !u32 {
    switch(self.*) {
      Instruction.cmd => |*cmd| return cmd.exec(env),
      Instruction.assignement => |*assignement| return assignement.exec(env),
      Instruction.ifInstruction => |*ifInstruction| return ifInstruction.exec(env),
    }
  }
};

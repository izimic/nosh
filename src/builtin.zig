const std = @import("std");
const ast = @import("./ast.zig");
const e = @import("./environement.zig");
const JsonParser = @import("./json.zig").JsonParser;
const stdout = std.io.getStdOut();
const stdin = std.io.getStdIn();

pub fn exec(name: [:0]const u8, args: [] ast.Arg, env: *e.Environement) !bool {
  if (std.mem.eql(u8,name,"cd")) {
    try cd(args,env);
  }
  else if (std.mem.eql(u8,name,"each")) {
    try each(args,env);
  }
  else if (std.mem.eql(u8,name,"select")) {
    try select(args,env);
  }
  else if (std.mem.eql(u8,name,"ls")) {
    try ls(args,env);
  }
  else {
    return false;
  }
  return true;
}

pub fn cd(args: [] ast.Arg, env: *e.Environement) !void {
  var path:[:0]const u8 = "";
  if (args.len > 0) {
    path = args[0].evaluate(env);
  }
  else {
    path = env.getString("HOME");
    if (std.mem.eql(u8,path,"")) {
      path = "/";
    }
  }
  try std.os.chdir(path);

  try env.put_global("OLDPWD",env.get("PWD"));

  var buf:[1024] u8= undefined;
  var slice = try std.os.getcwd(buf[0..]);
  try env.put_global("PWD",.{.String = slice});
}

pub fn each(args: [] ast.Arg, env: *e.Environement) !void {
  var gpa = std.heap.GeneralPurposeAllocator(.{}){};
  var arena = std.heap.ArenaAllocator.init(gpa.allocator());
  defer arena.deinit();
  const allocator = arena.allocator();

  var parser = JsonParser.init(stdin,allocator);

  var block: ?*ast.Block = null;

  if (args.len > 0) {
    switch(args[0]) {
      .block => |*b| block = b,
      else => {}
    }  
  } 
  
  while (try parser.parse()) |value| {
    try env.put_global(".",value);
    if (block) |b| {
      _ = try b.exec_alone(env);
    }
  }
}

pub fn select(args: [] ast.Arg, env: *e.Environement) !void {
  var gpa = std.heap.GeneralPurposeAllocator(.{}){};
  var arena = std.heap.ArenaAllocator.init(gpa.allocator());
  defer arena.deinit();
  const allocator = arena.allocator();

  var parser = JsonParser.init(stdin,allocator);

  var block: ?*ast.Block = null;

  if (args.len > 0) {
    switch(args[0]) {
      .block => |*b| block = b,
      else => {}
    }  
  }

  while (try parser.parse()) |value| {
    try env.put_global(".",value);
    if (block) |b| {
      if ((try b.exec_alone(env)) == 0) {
        try value.jsonStringify(.{},stdout.writer());
      }
    }
  }
}

pub fn ls(args: [] ast.Arg, env: *e.Environement) !void {
  _ = args;
  var dir = try std.fs.openDirAbsolute(env.getString("PWD"), std.fs.Dir.OpenDirOptions {
  .access_sub_paths = true,
    .iterate = true,
    .no_follow = false
  });

  var it = dir.iterate();
  const format =
    "{{" ++
    "\"type\": \"file\"," ++
    "\"name\": \"{s}\"," ++
    "\"kind\": \"{}\"" ++
    "}}\n"
  ;

  while (try it.next()) |file| {
    try std.fmt.format(stdout.writer(),format,.{ file.name, file.kind } );
  }

  dir.close();
}

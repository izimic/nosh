const std = @import("std");
const JsonValue = std.json.Value;
var gpa = std.heap.GeneralPurposeAllocator(.{}){};

pub const Environement = struct {
  allocator: std.mem.Allocator,
  local: std.StringHashMap(JsonValue),
  global: std.StringHashMap(JsonValue),

  pub fn init(allocator: std.mem.Allocator) !Environement {
    var env = Environement {
      .allocator = allocator,
      .local = std.StringHashMap(JsonValue).init(allocator),
      .global = std.StringHashMap(JsonValue).init(allocator),
    };

    var ptr = std.c.environ;
    while (ptr.*) |line| : (ptr += 1) {

      var line_i: usize = 0;
      while (line[line_i] != 0 and line[line_i] != '=') : (line_i += 1) {}
      var key = line[0..line_i+1];
      key[line_i] = 0;
      key = key[0..line_i:0];

      var end_i: usize = line_i+1;
      while (line[end_i] != 0) : (end_i += 1) {}
      const value = line[line_i + 1 .. end_i:0];

      try env.global.put(key,.{.String = value});
    }

    return env;
  }

  pub fn get(self: Environement,key: []const u8) JsonValue {
    if(self.global.get(key)) |val| {
      return val;
    }
    else if (self.local.get(key)) |val| {
      return val;
    }
    else {
      return JsonValue.Null;
    }
  }

  pub fn getString(self: Environement,key: []const u8) [:0]const u8 {
    const allocator = gpa.allocator();
    var buffer = allocator.alloc(u8, 1024) catch return "";
    var fbs = std.io.fixedBufferStream(buffer);
    switch (self.get(key)) {
      .String => |str| {
        _ = fbs.writer().write(str) catch return "";
      },
      .Null => return "null",
      else => |value| {
        value.jsonStringify(.{},fbs.writer()) catch return "null";
      }
    }
    fbs.writer().writeByte(0) catch return "";
    var len = fbs.getPos() catch return 1;
    return buffer[0..len-1:0];
  }

  pub fn put_local(self: *Environement, k: []const u8, v: JsonValue) !void {
    const key = try self.allocator.alloc(u8, k.len);
    std.mem.copy(u8,key,k);
    
    var value = try self.allocator.alloc(JsonValue, 1);
    value[0] = v;
    
    try self.local.put(key,value[0]);
  }

  pub fn put_global(self: *Environement, k: []const u8, v: JsonValue) !void {
    const key = try self.allocator.alloc(u8, k.len);
    std.mem.copy(u8,key,k);
    
    var value = try self.allocator.alloc(JsonValue, 1);
    value[0] = v;

    try self.global.put(key,value[0]);
  }

  pub fn toPtr(self: Environement) ![*:null]const ?[*:0]const u8 {
    const allocator = gpa.allocator();
    const env = try allocator.alloc(?[*:0]const u8, self.global.count() + 1);
    
    var it = self.global.iterator();

    var i:usize = 0;
    while(it.next()) |item| {
      var value = switch (item.value_ptr.*) {
        .String => |str| str,
        else => "{Object}"
      };
      var tmp = try allocator.alloc(u8, item.key_ptr.len + value.len+2);
      std.mem.copy(u8,tmp[0..],item.key_ptr.*);
      tmp[item.key_ptr.len] = '=';
      std.mem.copy(u8,tmp[item.key_ptr.len+1..],value);
      tmp[tmp.len-1] = 0;
      env[i] = tmp[0..tmp.len-1:0].ptr;
      i += 1;
    }

    env[i] = null;

    return env[0..env.len-1:null].ptr;
  }
};

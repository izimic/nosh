const std = @import("std");
const Value = std.json.Value;
const ValueTree = std.json.ValueTree;
const Token = std.json.Token;
const ObjectMap = std.json.ObjectMap;
const Array = std.json.Array;

pub const JsonParser = struct {
  file: std.fs.File,
  
  sp: std.json.StreamingParser,
  token: ?Token,

  allocator: std.mem.Allocator,
  str: std.ArrayList(u8),

  pub fn init(file: std.fs.File,allocator: std.mem.Allocator) JsonParser {
      return JsonParser {
          .file = file,
          .sp = std.json.StreamingParser.init(), 
          .token = null,
          .allocator = allocator,
          .str = std.ArrayList(u8).init(allocator),
      };
  }

  pub fn next(self: *JsonParser) !?Token {
    if (self.token) |token| {
        self.token = null;
        return token;
    }

    var reader = self.file.reader();
    var t1: ?Token = undefined;
    var t2: ?Token = undefined;

    while (true) {
      var byte = reader.readByte() catch break;
      try self.str.append(byte);
      try self.sp.feed(byte, &t1, &t2);

      if (t1) |token| {
        self.token = t2;
        return token;
      }
    }

    // Without this a bare number fails, the streaming parser doesn't know the input ended
    try self.sp.feed(' ', &t1, &t2);

    if (t1) |token| {
      return token;
    } else {
      return null;
    } 
  }

  pub fn parse(self: *JsonParser) !?Value {
    self.str.clearRetainingCapacity();
    self.sp.reset();

    if (try self.next()) |token| {
      return try self.parseTree(token);
    }

    return null;
  }

  fn parseTree(self: *JsonParser,token: Token) anyerror!Value {
    switch(token) {
      .ObjectBegin => return self.parseObject(),
      .ArrayBegin => return self.parseArray(),
      .String => |s| return self.parseString(s),
      .Number => |n| return self.parseNumber(n),
      .True => return self.parseTrue(),
      .False => return self.parseFalse(),
      .Null => return self.parseNull(),
      .ObjectEnd, .ArrayEnd => unreachable
    }
  }

  fn parseObject(self: *JsonParser) !Value {
    var object = ObjectMap.init(self.allocator);
    while(try self.next()) |token| {
      switch(token) {
        .ObjectEnd => return Value { .Object = object },
        .String => |s| {
          var key = (try self.parseString(s)).String;
          if (try self.next()) |tok| {
            var val = try self.parseTree(tok);
            try object.put(key,val);
          }
          else unreachable;
        },
        else => return error.InvalidLiteral,
      }
    }
    return Value { .Object = object}; 
  }

  fn parseArray(self: *JsonParser) !Value {
    var array = Array.init(self.allocator);
    while(try self.next()) |token| {
      if (token == .ArrayEnd) break;
      var val = try self.parseTree(token);
      try array.append(val);
    }
    return Value {.Array = array}; 
  }

  fn parseString(self: *JsonParser, token: std.meta.TagPayload(Token, Token.String)) !Value {
    var str = self.str.toOwnedSlice();
    var val = str[str.len-1-token.count..str.len-1];

    switch (token.escapes) {
      .None => return Value{ .String = val},
      .Some => {
        const output = try self.allocator.alloc(u8, token.decodedLength());
        errdefer self.allocator.free(output);
        try std.json.unescapeValidString(output, val);
        return Value{ .String = output };
      },
    }
  }

  fn parseNumber(self: *JsonParser, token: std.meta.TagPayload(Token, Token.Number)) !Value {
    var str = self.str.toOwnedSlice();
    var val = str[str.len-token.count..];

    return if (token.is_integer)
      Value{
        .Integer = std.fmt.parseInt(i64, val, 10) catch |e| switch (e) {
          error.Overflow => return Value{ .NumberString = val },
          error.InvalidCharacter => |err| return err,
        },
      }
    else
      Value{ .Float = try std.fmt.parseFloat(f64, val) };
  }

  fn parseTrue(self: *JsonParser) !Value {
    self.str.clearRetainingCapacity();
    return Value {.Bool = true};
  }

  fn parseFalse(self: *JsonParser) !Value {
    self.str.clearRetainingCapacity();
    return Value {.Bool = false};
  }

  fn parseNull(self: *JsonParser) !Value {
    self.str.clearRetainingCapacity();
    return Value.Null;
  }
}; 
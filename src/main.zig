const std = @import("std");
const parser = @import("./parser.zig");
const ast = @import("./ast.zig");
const e = @import("./environement.zig");

pub fn nosh(str: []const u8, env: *e.Environement) !void {
  var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
  defer arena.deinit();
  
  var p = parser.Parser.init(str,arena.allocator());

  var instructions = try p.readLine();

  for (instructions) |*instruction| {
    var status = try instruction.exec(env);
    std.debug.print("[return value {d}]\n", .{status});
  }
}

pub fn main() !void {
  var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
  defer arena.deinit();
  
  var env = try e.Environement.init(arena.allocator());

  const stdin = std.io.getStdIn();
  const stdout = std.io.getStdOut();
  var buffer:[512]u8 = undefined;

  std.debug.print("Coucou\n", .{});
  while(true) {
    _ = try stdout.write("$> ");
    var line = try stdin.reader().readUntilDelimiterOrEof(&buffer, '\n');

    if (line) |l| {
      try nosh(buffer[0..l.len+1], &env);
    }
    else {
      _ = try stdout.write("\nbye bye\n");
      return;
    }
  }
}

test "simple test" {
  var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
  var env = try e.Environement.init(arena.allocator());

  var cmds = [_][:0]const u8{
      "echo machin\n",
      "echo bidule truc | cat\n",
      "echo machin dibil | cat | cat\n",
      "truc=machin; echo $truc\n",
      "machin=bidule\n",
      "echo $machin\n",
      "pwd\n",
      "echo $n truc\n",
  };

  for (cmds) |cmd| {
    try nosh(cmd[0..cmd.len :0], &env);
  }

  arena.deinit();
}

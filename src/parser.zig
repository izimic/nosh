const std = @import("std");
const ast = @import("./ast.zig");

pub const Parser = struct {
  const ParseError = error {
    Unexpected,
    MissingCommand,
    MissingValue,
  };

  allocator : std.mem.Allocator,
  str : []const u8,
  index :usize = 0,

  pub fn init(str: []const u8, allocator: std.mem.Allocator) Parser {
    return Parser {
      .allocator = allocator,
      .str = str,
    };
  }

  pub fn getChar(self: Parser) u8 {
    return self.str[self.index];
  }

  pub fn consumeChar(self: *Parser) void {
    self.index += 1;
  }

  pub fn nextChar(self: *Parser) u8 {
    self.index = self.index + 1;
    return self.str[self.index];
  }

  pub fn readLine(self: *Parser) ![]ast.Instruction {
    var instructions = std.ArrayList(ast.Instruction).init(self.allocator);
    defer instructions.deinit();

    while(true) {
      var c = self.getChar();
      if (c == ';') {
        self.consumeChar();
        continue;
      }
      else if (c == '\n') {
        self.consumeChar();
        break;
      }
      var instruction = try self.readInstruction();
      if (instruction) |i| {
        try instructions.append(i);
      }
      else {
        return ParseError.Unexpected;
      }
    }
    return instructions.toOwnedSlice();
  }

  pub fn readInstruction(self: *Parser) anyerror!?ast.Instruction {
    self.readSpace();
    var mid = try self.readId();

    if (mid) |id| {
      if (std.mem.eql(u8,id,"if")) {
        return ast.Instruction {.ifInstruction = try self.readIf()};
      }
      else {
        self.readSpace();
        if (self.getChar() == '=') {
          if (try self.readAssignement(id)) |assignement| {
            return ast.Instruction { .assignement = assignement };
          }
        }
      }
    }
    
    if (try self.readCmd(mid)) |cmd| {
      return ast.Instruction { .cmd = cmd };
    }
    return null;
  }

  pub fn readIf(self: *Parser) !ast.IfInstruction {
    self.readSpace();
      if (self.getChar() == '(') {
        self.consumeChar();
        if(try self.readCmd(null)) |cmd| {
          if (self.getChar() == ')') {
            self.consumeChar();
            if (try self.readInstruction()) |ins| {
              var ains = try self.allocator.alloc(ast.Instruction,1);
              ains[0] = ins;
              return ast.IfInstruction.init(cmd,&ains[0]);
            }
            return ParseError.MissingCommand;
          }
          return ParseError.Unexpected;
        }
        return ParseError.MissingCommand;
      }
    return ParseError.Unexpected;
  }

  pub fn readAssignement(self: *Parser, name:[:0]u8) !?ast.Assignement {
    if (try self.readValue()) |val| {
      return ast.Assignement.init(name,val);
    }
    else {
      return ParseError.MissingValue;
    }
  }

  pub fn readCmd(self: *Parser, maybe_name: ?[:0]u8) anyerror!?ast.CmdInstruction {
    var processList = std.ArrayList(ast.Process).init(self.allocator);

    if (maybe_name) |name| {
      try processList.append( ast.Process.init(
        .{ .command = (try self.readCommandWithName(.{.id = name})).?}
      ));
    }
    else {
      if (try self.readProcess()) |p| {
        try processList.append(p);
      }
      else return ParseError.MissingCommand;
    }

    var c = self.getChar();

    while(true) {
      if (c == '|') {
        self.consumeChar();
        c = self.getChar();
        if (c == '|') {
          self.consumeChar();
          if (try self.readCmd(null)) |cmd| {
            var cmdInstruction = try self.allocator.alloc(ast.CmdInstruction,1);
            cmdInstruction[0] = cmd;
            return ast.CmdInstruction {
              .process = .{ .process = processList.toOwnedSlice() },
              .op = .{ .orCmd = &cmdInstruction[0] },
            };
          }
          else return ParseError.MissingCommand;
        }
        else {
          if (try self.readProcess()) |ncmd| {
            try processList.append(ncmd);
            continue;
          }
        }
      }
      else if (c == '&') {
        self.consumeChar();
        c = self.getChar();
        if ( c == '&' ) {
          self.consumeChar();
          if (try self.readCmd(null)) |cmd| {
            var cmdInstruction = try self.allocator.alloc(ast.CmdInstruction,1);
            cmdInstruction[0] = cmd;
            return ast.CmdInstruction {
              .process = .{ .process = processList.toOwnedSlice() },
              .op = .{.andCmd = &cmdInstruction[0]},
            };
          }
          else return ParseError.MissingCommand;
        }
        else {
          return ast.CmdInstruction {
            .process = .{ .process = processList.toOwnedSlice() },
            .background = true,
            .op = .Null,
          };
        }
      }
      else {
        return ast.CmdInstruction {
          .process = .{ .process = processList.toOwnedSlice() },
          .op = .Null,
        };
      }
    }
    unreachable;
  }

  pub fn readProcess(self: *Parser) !?ast.Process {
    if (try self.readBlock()) |block| {
      return ast.Process.init(.{.block = block});
    }
    else if (try self.readCommand()) |cmd| {
      return ast.Process.init(.{ .command = cmd});
    }
    else return null;
  }

  pub fn readBlock(self: *Parser) !?ast.Block {
    if (self.getChar() == '{') {
      self.consumeChar();
      //TODO Lines
      var instructions = std.ArrayList(ast.Instruction).init(self.allocator);
      while(self.getChar() != '}') {
        if (try self.readInstruction()) |ins| {
          try instructions.append(ins);
        }
      }

      self.consumeChar();

      return ast.Block.init(instructions.toOwnedSlice());
    }
    else return null;
  }

  pub fn readCommand(self: *Parser) !?ast.Command {
    if (try self.readValue()) |name| {
      return self.readCommandWithName(name);
    }
    else return null;
  }

  pub fn readCommandWithName(self: *Parser, name: ast.Value) !?ast.Command {
    var args = std.ArrayList(ast.Arg).init(self.allocator);

    while(try self.readArg()) |arg| {
      try args.append(arg);
    }
    return ast.Command.init(name,args.toOwnedSlice());
  }

  pub fn readArg(self: *Parser) !?ast.Arg {
    self.readSpace();
    if (try self.readBlock()) |block| {
      return ast.Arg{.block = block};
    }
    else if (try self.readValue()) |val| {
      return ast.Arg{.value = val};
    }
    else return null;
  }

  pub fn readValue(self: *Parser) !?ast.Value {
    self.readSpace();
    var c = self.getChar();
    if (try self.readVariable()) |variable| {
      return ast.Value { .variable = variable };
    }
    else if (c == '"') {
      var str = std.ArrayList(u8).init(self.allocator);
      while(true) {
        c = self.nextChar();
        if (c == '"') {
          self.consumeChar();
          return ast.Value { .id = try str.toOwnedSliceSentinel(0)};
        }
        else {
          try str.append(c);
        }
      }
    }
    else if (try self.readId()) |id| {
      return ast.Value { .id = id };
    }
    else return null;
  }

  pub fn readVariable(self: *Parser) !?ast.Variable {
    if (self.getChar() == '$') {
      self.consumeChar();
      var id = if (try self.readId()) |id| id else "";
      return ast.Variable.init(id);
    }
    else return null;
  }

  pub fn readId(self: *Parser) !?[:0]u8 {
    var str = std.ArrayList(u8).init(self.allocator);
    var c = self.getChar();

    if (isDelim(c)) return null;

    while(!isDelim(c)) {
      try str.append(c);
      c = self.nextChar();
    }
    return try str.toOwnedSliceSentinel(0);
  }

  pub fn readSpace(self: *Parser) void {
    while(isBlank(self.getChar())) {
      self.consumeChar();
    }
  }
};

fn isDelim(c:u8) bool {
  for("(){}=;&| \t\n") |i| {
    if (i == c) {
      return true;
    }
  }
  return (c == 0);
}

fn isBlank(c:u8) bool {
  return c == ' ' or c == '\t';
}
